#!/bin/bash
########################################
# Created by : yudikeren
# date : 27 Des 2019
# Delegated for : Inews.id on alibaba
# Update version : 3.1 (01/02/20)
########################################
path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
[ ! -f "$path/.step" ] && touch $path/.step && echo 0 > $path/.step
FileStep=$(cat $path/.step)
PCREversion=8.44
ZLIBversion=1.2.11
NGINXversion=1.18.0
OPENSSLversion=1.1.1i
PHPversion=7.3.26
RDSversion=0.3.9
MCversion=1.0.4
verOS=$(lsb_release -i |awk '{print$3}')
export LANGUAGE=en_US.UTF-8
export LC_ALL=en_US.UTF-8
echo "LC_ALL=en_US.UTF-8" > /etc/environment
echo "LANG=en_US.UTF-8" >> /etc/environment

confingLBNGINX()
{
echo ' truncate -s 0 /usr/local/nginx/conf/nginx.conf

worker_processes  auto;

events {
    worker_connections  1024;
}

stream{
    upstream stream_backend {
        server 172.31.17.130:3306 weight=1;
        server 172.31.17.212:3306 weight=1;
        server 172.31.21.28:3306 weight=1;
    }
    server {
#        server_name 172.31.25.205;
        listen 3306;
#       access_log  /var/log/nginx/logs/stream_access.log;
#        access_log      on;
#        error_log  /var/log/nginx/logs/stream_error.log;
        proxy_connect_timeout 3s;
        proxy_pass stream_backend;
    }
} ' | tee -a /usr/local/nginx/conf/nginx.conf  >> /dev/null 2>&1
}

confingNGINX()
{
truncate -s 0 /usr/local/nginx/conf/nginx.conf
echo ' worker_processes  auto;
events {
    worker_connections  1024;
}


http {
    include       mime.types;
    default_type  application/octet-stream;
    aio            on;
    sendfile        on;
    tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    server {
        listen       8080;
        server_name  localhost;

        #charset koi8-r;

        #access_log  logs/host.access.log  main;

                 location / {
            root   html;
            index  index.html index.htm;
        }

        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
        }
} ' | tee -a /usr/local/nginx/conf/nginx.conf  >> /dev/null 2>&1
}

printf "\nStarting Instalation Proses\n"

step1()
{
echo "install depedensis"

if [ $verOS = CentOS ];then

	echo $verOS
	echo "Add NGINX User"
	adduser --system --no-create-home --user-group -s /sbin/nologin nginx
   	yum groupinstall -y 'Development Tools' && yum install -y glibc-devel glibc-headers kernel-headers libgomp libpspell-dev libstdc++ libstdc-devel e2fsprogs-devel krb5-devel keyutils-libs-devel libselinux-devel libsepol-devel ncurses-devel automake autoconf flex freetype-devel imake libjpeg-devel libpng-devel libtool libxml2-devel libXpm-devel readline-devel bzip2-devel libcurl-devel db4-devel gdbm-devel mcrypt libmcrypt-devel epel-release xmlsec1-openssl-devel-1.2.20-4.el6.x86_64 perl perl-devel perl-ExtUtils-Embed libxslt libxslt-devel libxml2 libxml2-devel gd gd-devel GeoIP GeoIP-devel openssl-devel  aspell-devel openssl-devel libmcrypt-devel
   	
   	echo "Finished install"
   	echo 1 > $path/.step

elif [ $verOS = Ubuntu ];then

	echo "Add NGINX User"
	adduser --system --no-create-home --shell /bin/false --group --disabled-login nginx
	apt install -y build-essential libcurl4-openssl-dev libdb-dev libreadline-dev libpcre3-dev zlib1g-dev libssl-dev libatomic-ops-dev libxml2-dev libxslt1-dev libgeoip1 libgeoip-dev libgd-dev google-perftools libgoogle-perftools-dev libperl-dev libssl-dev libcurl4-openssl-dev pkg-config bison libbz2-dev libgdbm-dev zip
	cd /usr/local/include ; ln -s /usr/include/x86_64-linux-gnu/curl curl
    apt-get install libcurl4-gnutls-dev
	echo "Finished install"
   	echo 1 > $path/.step
else
	echo "Upss..!!! Sorry. OS Not Support Yet !"
	exit 1
fi
}



step2()
{
		# https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-open-source/
	   	# Instalasi nginx
	   	echo "Downloading Software to install"

		echo "Downloading init file and configure services"
        git clone https://gitlab.com/yudikeren/config.git

	   	echo "create nginx user"
	   	adduser --system --no-create-home --user-group -s /sbin/nologin nginx

	   	#https://ftp.pcre.org/pub/pcre/
	   	echo "Downloading pcre"
	   	wget https://ftp.pcre.org/pub/pcre/pcre-$PCREversion.tar.gz
	   	tar -zxf pcre-$PCREversion.tar.gz

	   	#https://zlib.net/
	   	echo "Downloading zlib"
	   	wget https://www.zlib.net/zlib-$ZLIBversion.tar.gz
	   	tar -zxf zlib-$ZLIBversion.tar.gz

	   	#https://www.openssl.org/source/
	   	echo "Downloading OPENSSL"
		wget https://www.openssl.org/source/openssl-$OPENSSLversion.tar.gz
		tar -zxf openssl-$OPENSSLversion.tar.gz

		#http://nginx.org/en/download.html
		echo "Downloading NGINX"
		wget https://nginx.org/download/nginx-$NGINXversion.tar.gz
	   	tar zxf nginx-$NGINXversion.tar.gz

		echo "Downloading PHP-fpm"
		wget https://www.php.net/distributions/php-$PHPversion.tar.gz
		tar -zxf php-$PHPversion.tar.gz

        echo "Downloading redis-modules"
        wget https://people.freebsd.org/~osa/ngx_http_redis-$RDSversion.tar.gz
        tar -zxf ngx_http_redis-$RDSversion.tar.gz

		echo "starting install mongodb plugin on php-fpm"
		git clone https://github.com/mongodb/mongo-php-driver.git
		cd $path/mongo-php-driver; git submodule update --init

		wget https://pecl.php.net/get/mcrypt-$MCversion.tgz
        tar -zxf mcrypt-$MCversion.tgz
	   	echo "Finished downloading software"
	   	sleep 1
	   	echo 2 > $path/.step

}

step3()
{
	   echo "Compailing Software"
	   echo "compiling NGINX"

	   cd $path/nginx-$NGINXversion
	   ./configure --user=nginx --group=nginx --prefix=/usr/local/nginx --sbin-path=/usr/local/nginx/sbin/nginx --conf-path=/usr/local/nginx/conf/nginx.conf --pid-path=/var/run/nginx/nginx.pid --lock-path=/var/lock/nginx.lock --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --with-http_ssl_module --with-http_image_filter_module --with-http_flv_module --with-http_gzip_static_module --with-http_addition_module --with-http_sub_module --with-http_dav_module --with-http_mp4_module --with-http_gzip_static_module --with-http_random_index_module --with-http_secure_link_module --with-http_stub_status_module --with-mail=dynamic --with-stream --with-mail_ssl_module --with-file-aio --with-ipv6 --with-openssl=$path/openssl-$OPENSSLversion --error-log-path=/usr/local/nginx/log/logs --http-client-body-temp-path=/usr/local/nginx/client_body_temp/ --http-proxy-temp-path=/usr/local/nginx/proxy_temp/ --http-fastcgi-temp-path=/usr/local/nginx/fastcgi_temp/ --with-pcre=$path/pcre-$PCREversion --with-pcre-jit --with-zlib=$path/zlib-$ZLIBversion --with-debug --with-file-aio
	   echo "Installing NGINX"
	   cd $path/nginx-$NGINXversion; make
	   cd $path/nginx-$NGINXversion; make install
	   echo "create default config"
	   confingNGINX
	   printf "\nNGINX Installation Completed\n"
	   echo 3 > $path/.step
}

step4()
{
	   echo "Starting Install PHP-fpm"
	   echo "Compiling PHP-fpm"
	   cd $path/php-$PHPversion
	   './configure' '--libdir=/usr/lib' '--with-libdir=lib' '--prefix=/usr/local/php' '--sysconfdir=/usr/local/php/etc'   '--with-config-file-path=/etc'   '--enable-mod-charset'   '--enable-fpm'   '--enable-libgcc'   '--with-libxml-dir=/usr/lib64'   '--with-openssl'   '--with-zlib-dir=/usr/lib64'   '--with-bz2'   '--enable-calendar'   '--with-jpeg-dir=/usr/lib64'   '--with-mcrypt=/usr/bin/mcrypt'   '--with-mhash=/usr/local/lib'   '--with-gdbm=/usr/include'   '--with-curl=/usr/lib64/'   '--enable-exif'   '--enable-bcmath'   '--enable-ftp'   '--with-gd'   '--with-png-dir=/usr/lib64'   '--with-xpm-dir=/usr/lib64'   '--with-freetype-dir=/usr/include/freetype2/freetype'   '--enable-gd-native-ttf'   '--enable-mbstring'   '--enable-cli'   '--enable-gd-jis-conv'   '--with-gettext'   '--with-kerberos'   '--with-mysqli=mysqlnd'   '--with-mysql-sock'   '--with-libdir=lib64'   '--enable-pcntl'   '--with-readline=/usr/include/readline'   '--enable-soap'   '--enable-sockets'   '--with-pear'   '--enable-maintainer-zts'   '--with-xmlrpc'   '--with-libxml-dir=/usr/local/lib'   '--with-pdo-mysql'   '--enable-opcache' '--enable-dba'  '--with-db5=/usr/include/db5'
	   cd $path/php-$PHPversion; make -j 4
	   cd $path/php-$PHPversion; make install
	   printf "\nPHP-fpm Instalation Completed\n"
	   echo 4 > $path/.step
}

step5()
{
	   echo "starting install mongodb plugin on php-fpm"
	   cd $path/mongo-php-driver; git submodule update --init
	   /usr/local/php/bin/phpize
	   cd $path/mongo-php-driver; ./configure --with-php-config=/usr/local/php/bin/php-config
	   cd $path/mongo-php-driver; make
	   cd $path/mongo-php-driver; make install
	   echo "Copy configurtion mongodb to php.ini"
       echo 5 > $path/.step
}

step6()
{
	   echo "Downloading init file and configure services"
	   cp $path/php-$PHPversion/php.ini-production /etc/php.ini
       echo extension=mongodb.so >> /etc/php.ini
	   chmod +x $path/config/run.sh
	   rm -rf /usr/local/php/etc
	   cp $path/config/php-fpm /etc/init.d/
       chmod +x /etc/init.d/php-fpm
	   mv $path/config/etc /usr/local/php
	   mv $path/config/run.sh /opt/
	   mv config/webnginx.service /etc/systemd/system
	   mv config/webphp.service /etc/systemd/system
	   systemctl enable webnginx && systemctl enable webphp
	   systemctl start webnginx && systemctl start webphp

	   echo 6 > $path/.step
}

step7()
{
	   echo "starting install mcrypt plugin on php-fpm"
	   cd $path/mcrypt-$MCVersion; git submodule update --init
	   /usr/local/php/bin/phpize
	   cd $path/mcrypt-$MCVersion; ./configure --with-php-config=/usr/local/php/bin/php-config
	   cd $path/mcrypt-$MCVersion; make
	   cd $path/mcrypt-$MCVersion; make install
	   echo "Copy configurtion mcrypt to php.ini"
       echo 7 > $path/.step
}

step8()
{
	   echo "Compailing Software"
	   echo "compiling NGINX"

	   cd $path/nginx-$NGINXversion
		./configure --user=nginx --group=nginx --prefix=/usr/local/nginx --add-dynamic-module=$path/ngx_http_redis-$RDSversion --sbin-path=/usr/local/nginx/sbin/nginx --conf-path=/usr/local/nginx/conf/nginx.conf --pid-path=/var/run/nginx/nginx.pid --lock-path=/var/lock/nginx.lock --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --with-http_ssl_module --with-http_image_filter_module --with-http_flv_module --with-http_gzip_static_module --with-http_addition_module --with-http_sub_module --with-http_dav_module --with-http_mp4_module --with-http_gzip_static_module --with-http_random_index_module --with-http_secure_link_module --with-http_stub_status_module --with-mail=dynamic --with-stream --with-mail_ssl_module --with-file-aio --with-ipv6 --with-openssl=$path/openssl-$OPENSSLversion --error-log-path=/usr/local/nginx/log/logs --http-client-body-temp-path=/usr/local/nginx/client_body_temp/ --http-proxy-temp-path=/usr/local/nginx/proxy_temp/ --http-fastcgi-temp-path=/usr/local/nginx/fastcgi_temp/ --with-pcre=$path/pcre-$PCREversion --with-pcre-jit --with-zlib=$path/zlib-$ZLIBversion --with-debug --with-file-aio
	   echo "Installing LB NGINX REDIS"
       cd $path/nginx-$NGINXversion; make
       cd $path/nginx-$NGINXversion; make install
	   printf "\nNGINX Installation Completed\n"
	   echo 8 > $path/.step
}

if [ $FileStep = 0 ];then
	step1
	step2
	step3
	step4
	step5
	step6
	step7
elif [ $FileStep = 1 ];then
	step2
	step3
	step4
	step5
	step6
	step7
elif [ $FileStep = 2 ];then
	step3
	step4
	step5
	step6
	step7
elif [ $FileStep = 3 ];then
	step4
	step5
	step6
	step7
elif [ $FileStep = 4 ];then
	step5
	step6
	step7
elif [ $FileStep = 5 ];then
	step6
	step7
elif [ $FileStep = 6 ];then
	step7
else
     echo "Installation Has Been DONE !"
fi
